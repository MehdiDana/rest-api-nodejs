var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET presets listing. */
router.get('/', function(request, response, next) {
  fs.readFile( __dirname + "/../data/" + "presets.json", 'utf8', function (err, data) {
    response.writeHead(200, {'Content-Type' : 'application/json'});
    response.write(data);
    response.end();
  });
});

/* GET presets listing */
router.get('/list', function(request, response, next) {
  fs.readFile( __dirname + "/../data/" + "presets.json", 'utf8', function (err, data) {
    response.writeHead(200, {'Content-Type' : 'application/json'});
    response.write(data);
    response.end();
  });
});

/* Add new preset  */
router.post('/add', function (request, response) {
  // First read existing data
  var filename =  __dirname + "/../data/" + "presets.json";
  fs.readFile(filename, 'utf8', function (err, data) {
    var id = Math.random().toString(36).substring(10);
    var postData = request.body;
    try {
      data = JSON.parse( data );
    } catch (error) {
      data = {};
    }
    data[id] = postData;

    console.log( postData );
    fs.writeFile(filename, JSON.stringify(data), 'utf8');
    response.writeHead(200, {'Content-Type' : 'application/json'});
    response.write(JSON.stringify({id : id, 'added' : true}));
    response.end();
  });

});

/* delete a preset */
router.delete('/delete', function (request, response) {
  var id = request.body.id;
  var filename =  __dirname + "/../data/" + "presets.json";
  fs.readFile(filename, 'utf8', function (err, data) {
    try {
      data = JSON.parse( data );
    } catch (error) {
      data = {};
    }
    delete data[id];
    fs.writeFile(filename, JSON.stringify(data), 'utf8');
    console.log( data );
    response.writeHead(200, {'Content-Type' : 'application/json'});
    response.write(JSON.stringify({id : id, 'deleted' : true}));
    response.end();
  });
});

/* update a preset */
router.put('/update/:id', function (request, response) {
  var id = request.params.id;
  var filename =  __dirname + "/../data/" + "presets.json";
  fs.readFile(filename, 'utf8', function (err, data) {
    try {
      data = JSON.parse( data );
    } catch (error) {
      data = {};
    }
    data[id] = request.body;
    fs.writeFile(filename, JSON.stringify(data), 'utf8');
    console.log( data );
    response.writeHead(200, {'Content-Type' : 'application/json'});
    response.write(JSON.stringify({id : id, 'updated' : true}));
    response.end();
  });
});

/* Display by id */
router.get('/view/:id', function (request, response) {
  // First read existing preset
  var id = request.params.id;
  fs.readFile( __dirname + "/../data/" + "presets.json", 'utf8', function (err, data) {
    data = JSON.parse( data );
    var preset = data[id];
    console.log( preset );
    response.writeHead(200, {'Content-Type' : 'application/json'});
    response.write(JSON.stringify(preset));
    response.end();
  });
});

module.exports = router;