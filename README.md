# README #

This is a simple REST API created with Nodejs.

The data will be saved in a json file rather than database `just for simplicity`

**Endpoints are:**

* http://127.0.0.1:3001/api/presets/list
* http://127.0.0.1:3001/api/presets/add
* http://127.0.0.1:3001/api/presets/delete
* http://127.0.0.1:3001/api/presets/view/:id
* http://127.0.0.1:3001/api/presets/update/:id

The request's body must be a valid json and response is always json.
